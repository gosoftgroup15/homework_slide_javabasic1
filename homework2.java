package homework;

public class homework2 {
	public static void main(String[] args) {
		TableN();
		System.out.println("<----------------^-------------->");
		TableN2();
	}

	public static void TableN() {
		String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
		int[] newTable = new int[table.length];
		for (int row = 0; row < table.length; row++) {
			for (int element1 = 0; element1 < table.length; element1++) {
				newTable[row] = Integer.parseInt(table[row][element1]);
				System.out.printf("%3d", newTable[row]);
			}
			System.out.println("");
		}
	}

	public static void TableN2() {
		String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
		int[] newTable = new int[table.length];
		for (int row = 0; row < table.length; row++) {
			for (int element1 = 0; element1 < table.length; element1++) {
				newTable[row] = Integer.parseInt(table[row][element1]);
				System.out.printf("%3d", newTable[row] * 2);
			}
			System.out.println("");
		}
	}
}
